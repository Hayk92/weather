# Weather App

---

## Installation

## Server app

For runnig API server you need to do
1. `cd weather_api`
2. `composer install`
3. `php artisan key:generate`
4. `php artisan migrate --seed` for creating tables and fill Cities in to cities table
5. `php artisan passport:install` for creation Client API key
6. `php artisan serve` for starting php API server "default http://127.0.0.1:8000"
7. `php artisan queue:work` for listening jobs
8. `php artisan weather:importer` command for call openWeather API and get data

##Real time updating

For real time updating we using Redis and Socket io.
You need to configure redis, you can find configuration in `.env` file
Need to change Broadcast driver and Queue connection to redis 

**for example**

`BROADCAST_DRIVER=redis`

`QUEUE_CONNECTION=redis`

---

##Configuring [REDIS](https://redis.io/)

1. on .env you can find REDIS_HOST, REDIS_PASSWORD, REDIS_PORT
2. you can test by run redis-cli and write on cmd PING if returns PONG then redis is ran successfully

##Configuring Mail
For mail sending for example "Forgot password" you need to set mail configurations
in *.env* file

`MAIL_DRIVER=smtp`

`MAIL_HOST=smtp.mailtrap.io`
 
`MAIL_PORT=2525`
 
`MAIL_USERNAME=null`
 
`MAIL_PASSWORD=null`
 
`MAIL_ENCRYPTION=null` 

---

##OpenWeatherMap
For weather info we using [OpenWeatherMap 5 day / 3 hour forecast](https://openweathermap.org/forecast5)
API 
For setting your own [OpenWeatherMap](https://openweathermap.org) API key 
you need to configure in *.env* file

`OPEN_WEATHER_URL=http://api.openweathermap.org`

`OPEN_WEATHER_API_KEY=YOUR_API_KEY`

---

## Client app

1. `cd weather-front`
2. `npm install`
3. For runing Socket you need to run `npm run socket`
4. set api URL in env file 'enviroments/enviroments.ts' `api_url` Laravel (default http://127.0.0.1:8000)
5. set socket HOST in env file 'enviroments/enviroments.ts' `socket_url` (default http://localhost)
6. set socket PORT in env file 'enviroments/enviroments.ts' `socket_port` (default 3001)
7. for socket server set PORT in socket_config.js file `port` (default 3001)
8. `ng serve`

##TESTING

1. Create New MySql
2. .env set configurations
3. `DB_CONNECTION_TESTING`, `DB_HOST_TESTING`, `DB_PORT_TESTING`, `DB_DATABASE_TESTING`, `DB_USERNAME_TESTING`, `DB_PASSWORD_TESTING`
4 .open phpunit.xml config file <env> and change configuration
5 .php artisan migrate --database="mysql_testing" --seed
6. for running test ./vendor/bin/phpunit

Default using sqlite
` <server name="DB_CONNECTION" value="sqlite_testing"/>`