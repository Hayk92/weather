<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file_path = public_path('city_list/cities.json');
    	if (file_exists($file_path)){
    		$data = json_decode(file_get_contents($file_path), true);
		    foreach ($data as $datum){
			    $coord = $datum['coord'] ?? [];
			    unset($datum['coord']);
			    \App\Models\City::query()->create($datum + $coord);
		    }
	    }
    }
}
