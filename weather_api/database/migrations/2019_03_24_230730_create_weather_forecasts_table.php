<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_forecasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('weather_id')->unsigned();
            $table->bigInteger('forecast_id')->unsigned();

	        $table->foreign('weather_id')->references('id')
		        ->on('weathers')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');

	        $table->foreign('forecast_id')->references('id')
		        ->on('forecasts')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_forecasts');
    }
}
