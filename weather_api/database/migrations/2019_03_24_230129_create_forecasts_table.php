<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forecasts', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->bigInteger('date_time')->nullable();
	        $table->dateTime('date_text')->nullable();
	        $table->float('temp')->nullable();
	        $table->float('temp_min')->nullable();
	        $table->float('temp_max')->nullable();
	        $table->float('pressure')->nullable();
	        $table->float('sea_level')->nullable();
	        $table->float('grnd_level')->nullable();
	        $table->integer('humidity')->nullable();
	        $table->integer('temp_kf')->nullable();
	        $table->integer('clouds')->nullable()->default(0);
	        $table->float('wind_speed')->nullable();
	        $table->float('wind_deg')->nullable();
	        $table->float('rain')->nullable();
	        $table->float('snow')->nullable();
	        $table->bigInteger('city_id')->unsigned()->nullable();
	        $table->date('day')->nullable();
            $table->timestamps();

	        $table->foreign('city_id')->references('id')
		        ->on('cities')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecasts');
    }
}
