<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use LRedis;
Route::get('/', function () {
    // -0.61 -0.03
    event(new App\Events\GetNewWeather(588335));
    // event(new App\Events\CityWeather(588335));

    return view('welcome');
});

// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');