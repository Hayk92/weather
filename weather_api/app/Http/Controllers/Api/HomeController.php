<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Forecast;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::query()->with(['forecast' => function($query) use($request){
            $query->select('forecasts.*')
                ->where('day', Carbon::now()->format('Y-m-d'))
                ->currentThreeHours();
        }, 'forecast.weathers'])->get();

    	return response()->json(['cities' => $cities], 200);
    }

    public function getWeathers(Request $request)
	{
        $weather = Forecast::query()
            ->select(['forecasts.*'])
            ->where('day', Carbon::now()->format('Y-m-d'))
            ->filter($request->all())
            ->currentThreeHours()
			->with(['weathers', 'daily_forecasts' => function($query) use ($request){
                $query->select(['city_id', 'day', 'temp', 'date_text', 'clouds', 'rain'])
                    ->where('day', Carbon::now()->format('Y-m-d'))
                    ->orderBy('date_text', 'ASC');
            }, 'daily_forecasts.city' => function($query){
                $query->select('name', 'id');
            }])
            ->first();

    	$weathers = Forecast::query()
		    ->select(['forecasts.*'])
            ->selectRaw('
                IF(day = "'.Carbon::now()->format('Y-m-d').'", 1, 0) as today,
		        (SELECT min(f.temp) FROM forecasts as f WHERE f.day = forecasts.day AND f.city_id = forecasts.city_id) as min_temp_day, 
		        (SELECT max(f.temp) FROM forecasts as f WHERE f.day = forecasts.day AND f.city_id = forecasts.city_id) as max_temp_day')
			->filter($request->all())
            ->with(['weathers'])
            ->currentHourEachDayOfWeek()
            ->orderBy('day', 'ASC')
			->get();
			
		return response()->json(['today' => $weather, 'weathers' => $weathers], 200);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $week_day = new Carbon($request->day);
        $week_day = $week_day->format('Y-m-d');
        $weather = Forecast::query()
            ->select(['forecasts.*'])
            ->currentThreeHours($request->offset, $request->day)
			->with(['weathers', 'daily_forecasts' => function($query) use($week_day){
                $query->select(['city_id', 'day', 'temp', 'date_text', 'clouds', 'rain'])
                    ->where('day', $week_day)
                    ->orderBy('date_text', 'ASC');
            }, 'daily_forecasts.city' => function($query){
                $query->select('name', 'id');
            }])
            ->where('city_id', $request->city)
            ->orderBy('date_text', 'DESC')
            ->firstOrFail();
            
        return response()->json($weather, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
