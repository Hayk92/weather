<?php

namespace App\Traits;
use Illuminate\Support\Facades\Auth;

trait ForecastTrait
{
	public function scopeFilter($query, $data = [])
	{
		$data = !empty($data) ? ['city_id' => $data] : ['city_id' => Auth::user()->city_id];
		return $query->where($data);
	}

	public function scopeCurrentThreeHours($query, $offset = 0, $other = null)
	{
		$hour = !is_null($other) ? new \Carbon\Carbon($other) : \Carbon\Carbon::now();
		$offset = 0;
		$now = \Carbon\Carbon::now()->subHour($offset);
		return is_null($other) ? $query->whereRaw("
				(TIME( date_text ) >= TIME( '$now' ) - INTERVAL 3 HOUR
				AND TIME( date_text ) < TIME( '$now' ) + INTERVAL 3 HOUR)")
			: $query->whereRaw("date_text = '$hour'");
	}

	public function scopeWeeklyForecasts($query)
	{
		$date = \Carbon\Carbon::now();
	    $start = $date->subDays(1)->toDate()->format('Y-m-d');
        $end = $date->addDays(5)->toDate()->format('Y-m-d');
		return $query->whereBetween('day', [$start, $end]);
	}

	public function scopeCurrentHourEachDayOfWeek($query, $offset = 0)
	{
		$offset = 0;
		$now = \Carbon\Carbon::now()->subHour($offset);
		return $query->whereRaw(
		    "(date_text >= '$now' - INTERVAL 1 DAY 
			AND date_text < '$now' + INTERVAL 5 DAY )
			AND (TIME( date_text ) >= TIME( '$now' ) - INTERVAL 3 HOUR
			AND TIME( date_text ) < TIME( '$now' ))"
		);
	}
}