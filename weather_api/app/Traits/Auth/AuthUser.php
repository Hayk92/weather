<?php
namespace App\Traits\Auth;

use App\Http\Requests\UserRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait AuthUser
{
	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'city_id' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed',
		]);
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()], 422);
		}
		$data = $request->all();
		$data['password'] = bcrypt($data['password']);
		$user = User::create($data);
		$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
		$success['city'] = $user->city_id;
		$success['email'] = $user->email;
		return response()->json(['success' => $success], 200);
	}

	public function login()
	{
		if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){
			$user = Auth::user();
			$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
			$success['city'] = $user->city_id;
			$success['email'] = $user->email;
			return response()->json(['success' => $success], 200);
		}else{
			return response()->json(['errors' => ['email' => 'The user credentials is invalid!']], 422);
		}
	}

	public function logout()
	{
		Auth::user()->tokens()->delete();
		return response()->json(['success' => 'Successfully logged out!'], 200);
	}

	public function resetPassword(ResetPasswordRequest $request)
	{
		$user = User::query()->where('email', $request->email)->firstOrFail();
		$user->password = bcrypt($request->password);
		$user->save();
		$success['token'] = $user->createToken(env('APP_NAME'))->accessToken;
		$success['city'] = $user->city_id;
		$success['email'] = $user->email;
		return response()->json(['success' => $success], 200);
	}
}