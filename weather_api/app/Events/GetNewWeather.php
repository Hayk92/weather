<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Resources\ForecastResource;
use App\Models\Forecast;

class GetNewWeather implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $forecast;
    public $weathers;
    public $city_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($city_id)
    {
        $this->city_id = $city_id;
        $this->forecast = Forecast::query()
        ->select(['forecasts.*'])
        ->where('day', \Carbon\Carbon::now()->format('Y-m-d'))
        ->filter([$this->city_id])
        ->currentThreeHours()
        ->with(['weathers', 'daily_forecasts' => function($query) {
            $query->select(['city_id', 'day', 'temp', 'date_text', 'clouds', 'rain'])
                ->where('day', \Carbon\Carbon::now()->format('Y-m-d'))
                ->orderBy('date_text', 'ASC');
        }, 'daily_forecasts.city' => function($query){
            $query->select('name', 'id');
        }])
        ->first();
        
        $this->weathers = Forecast::query()
            ->select(['forecasts.*'])
            ->selectRaw('
                IF(day = "'.\Carbon\Carbon::now()->format('Y-m-d').'", 1, 0) as today,
                (SELECT min(f.temp) FROM forecasts as f WHERE f.day = forecasts.day AND f.city_id = forecasts.city_id) as min_temp_day, 
                (SELECT max(f.temp) FROM forecasts as f WHERE f.day = forecasts.day AND f.city_id = forecasts.city_id) as max_temp_day')
            ->filter([$this->city_id])
            ->with(['weathers'])
            ->currentHourEachDayOfWeek()
            ->orderBy('day', 'ASC')
            ->get();

        $this->forecast = new ForecastResource($this->forecast);
        $this->weathers = new ForecastResource($this->weathers);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['weather.'.$this->city_id];
    }
}
