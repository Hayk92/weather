<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Resources\CityResource;
use App\Models\City;

class CityWeather implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $cities;
    public $city_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($city_id)
    {
        $this->city_id = $city_id;
        $this->cities = new CityResource(
            City::query()->where('id', $this->city_id)
            ->with(['forecast' => function($query){
                $query->select('forecasts.*')
                    ->where('day', \Carbon\Carbon::now()->format('Y-m-d'))
                    ->currentThreeHours();
            } , 'forecast.weathers'])->first()
        );
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['cities'];
    }
}
