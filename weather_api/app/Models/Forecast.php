<?php

namespace App\Models;

use App\Traits\ForecastTrait;
use Illuminate\Database\Eloquent\Model;

class Forecast extends Model
{
	use ForecastTrait;

	protected $fillable = [
		'date_time',
		'date_text',
		'temp',
		'temp_min',
		'temp_max',
		'pressure',
		'sea_level',
		'grnd_level',
		'humidity',
		'temp_kf',
		'clouds',
		'wind_speed',
		'wind_deg',
		'rain',
		'snow',
		'city_id',
		'day',
	];

	public function weathers()
	{
		return $this->belongsToMany(Weather::class, 'weather_forecasts', 'forecast_id', 'weather_id');
	}

	public function daily_forecasts()
	{
		return $this->hasMany(Forecast::class, 'city_id', 'city_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id', 'id');
	}
}
