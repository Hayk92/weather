<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
		'name',
        'country',
        'lon',
        'lat',
    ];
    
    public function forecast()
    {
        return $this->hasOne(Forecast::class, 'city_id', 'id');
    }
}
