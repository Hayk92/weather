<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{

    protected $fillable = [
	    'weather_id',
		'main',
		'description',
		'icon',
    ];

    protected $appends = ['image'];

    public function getImageAttribute()
    {
    	return asset($this->icon);
    }

    public function forecast()
    {
    	return $this->belongsToMany(Forecast::class, 'weather_forecast', 'forecast_id', 'weather_id');
    }
}
