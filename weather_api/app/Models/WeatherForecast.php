<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeatherForecast extends Model
{
    protected $fillable = [
		'weather_id',
		'forecast_id',
    ];

    public $timestamps = false;
}
