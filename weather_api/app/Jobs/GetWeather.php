<?php

namespace App\Jobs;

use App\Events\GetNewWeather;
use App\Models\Forecast;
use App\Models\Weather;
use App\Models\WeatherForecast;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetWeather implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $list;
	private $city_id;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($list = [], $city_id = null)
	{
		$this->list = $list;
		$this->city_id = $city_id;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if (!empty($this->list)) {
			$weather_id = null;
			$forecast = $this->createForecast();
			if (!empty($this->list['weather'])) {
				$this->createWeather($forecast->id);
			}
		}
	}

	public function createForecast()
	{
		$day = new Carbon($this->list['dt']);
		$day = $day->format('Y-m-d');
		return Forecast::query()->updateOrCreate(
			[
				'date_time' => $this->list['dt'],
				'city_id' => $this->city_id,
			],
			$this->list['main'] +
			[
				'temp' => $this->list['main']['temp'],
				'clouds' => $this->list['clouds']['all'] ?? null,
				'wind_speed' => $this->list['wind']['speed'] ?? null,
				'wind_deg' => $this->list['wind']['deg'] ?? null,
				'rain' => $this->list['rain']['3h'] ?? null,
				'snow' => $this->list['snow']['3h'] ?? null,
				'date_text' => $this->list['dt_txt'] ? Carbon::createFromFormat('Y-m-d H:i:s', $this->list['dt_txt'])->setTimezone('UTC') : null,
				'day' => $day,
			]
		);
	}

	public function createWeather($forecast_id)
	{
		$count = count($this->list['weather']);
		for ($i = 0; $i < $count; $i++) {
			$weather = $this->list['weather'][$i];
			unset($weather['id']);
			$this->fetchImage($weather['icon'].'.png');
			$weather['icon'] = 'icons/'.$weather['icon'].'.png';
			$newWeather = Weather::query()->firstOrCreate(
				[
					'weather_id' => $this->list['weather'][$i]['id']
				],
				$weather
			);

			$this->fillPivotRelationship($forecast_id, $newWeather->id);
		}
	}

	public function fillPivotRelationship($forecast_id, $weather_id)
	{
		WeatherForecast::query()->updateOrCreate([
			'forecast_id' => $forecast_id,
			'weather_id' => $weather_id,
		],[
			'weather_id' => $weather_id,
			'forecast_id' => $forecast_id
		]);
	}

	public function fetchImage($image)
	{
		$path = public_path('icons/'.$image);
		if (!file_exists($path)){
			$icon = fopen($path, 'w');
			file_put_contents($path, file_get_contents("http://openweathermap.org/img/w/".$image));
		}
	}
}
