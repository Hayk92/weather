<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CronImitation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:imitation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will be run Weather importer every 10 minutes.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // while(true){
        //     \Artisan::call('weather:import');
        //     sleep(10*60);
        // }
    }
}
