<?php

namespace App\Console\Commands;

use App\Events\CityWeather;
use App\Events\GetNewWeather;
use App\Jobs\GetWeather;
use App\Models\City;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class WeatherImporter extends Command
{
	private $url;
	private $api_key;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will be call to Open Weather API and get data.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->url = env('OPEN_WEATHER_URL');
        $this->api_key = env('OPEN_WEATHER_API_KEY');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::query()->pluck('id');
        $client = new Client();

        foreach ($cities as $city){     
	        $url = $this->url . "/data/2.5/forecast?id={$city}&units=metric&APPID={$this->api_key}";

	        $response = $client->request('GET', $url);
            $result   = $response->getBody();

	        if ($response->getStatusCode() === 200) {
                $this->storeWeathers($result, $city);       
            }

            event(new CityWeather($city));
            event(new GetNewWeather($city));
        }
    }

    public function storeWeathers($result, $city_id)
    {
        $result = json_decode($result, true);
        if (!empty($result['list'])){
            foreach ($result['list'] as $value){
                dispatch(new GetWeather($value, $city_id))->onConnection('sync');
            }
        }
    }
}
