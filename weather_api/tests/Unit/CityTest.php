<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    protected $model = \App\Models\User::class;
    protected $client, $user, $token;

    public function createUser()
    {
        return factory($this->model)->create();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCitiesForRegister()
    {
    	$res = $this->json('GET', 'api/v1/cities');
    	$res->assertStatus(200);
    	$res->assertJsonStructure(['*' => [
            'country',
            'created_at',
            'id',
            'lat',
            'lon',
            'name',
            'updated_at'
        ]]);
    }

	public function testCitiesUnauthorized()
	{
		$res = $this->json('GET', 'api/v1/home');
		$res->assertStatus(401);
    }
    
	public function testCitiesAuthorized()
	{
        $this->user = $this->createUser();
        $this->user = $this->user->toArray();
        $body = [
            'email' => $this->user['email'],
            'password' => 'password'
        ];
        $this->user = $this->json('POST','/api/v1/login', $body, ['Content-Type' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['success' => ['token', 'city', 'email']]);

        $res = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. $this->user->original['success']['token']
        ])
        ->json('GET', '/api/v1/home')
            ->assertStatus(200)
            ->assertJsonStructure([
                   'cities' => [
                       '*' => [
                        'country',
                        'created_at',
                        'id',
                        'lat',
                        'lon',
                        'name',
                        'updated_at',
                        'forecast'
                    ]
                ]
            ]);

        $this->json('POST', '/api/v1/logout', [], ['Content-Type' => 'application/json', 'Authorization' => 'Bearer '. $this->user->original['success']['token']])
                ->assertStatus(200)
                ->assertJsonStructure(['success']);
    }
}
