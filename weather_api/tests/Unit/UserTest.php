<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    protected $model = \App\Models\User::class;
    protected $client, $user, $token;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function createUser()
    {
        return factory($this->model)->create();
    }

    public function testRegisterFailed()
    {
    	$user = [];
    	$this->json('POST', 'api/v1/register', $user)
		    ->assertJson($user)
		    ->assertStatus(422);
    }

    public function testRegister() 
    {
        $user = [
            "name" => "Dr. Winston Reynolds",
            "email" => "meghan.mcdermott@example.net",
            "password" => "password",
            "password_confirmation" => "password",
            "city_id" => "588335"
        ];

        $res = $this->json('POST', 'api/v1/register', $user);
        
        $res->assertStatus(200)
            ->assertJsonStructure(['success' => ['token', 'city', 'email']]);
    }
    
    public function testApiLoginFailed() {
        $this->user = $this->createUser();
        $this->user = $this->user->toArray();
        $body = [ ];
        $this->json('POST','/api/v1/login', $body, ['Content-Type' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonStructure(['errors' => ['email']]);
    }

    public function testApiLogin() {
        $this->user = $this->createUser();
        $this->user = $this->user->toArray();
        $body = [
            'email' => $this->user['email'],
            'password' => 'password'
        ];
        $this->json('POST','/api/v1/login', $body, ['Content-Type' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['success' => ['token', 'city', 'email']]);
    }

    public function testApiLogoutFailed() {
        $this->user = $this->createUser();
        $this->user = $this->user->toArray();
        $body = [
            'email' => $this->user['email'],
            'password' => 'password'
        ];
        $this->user = $this->json('POST','/api/v1/login', $body, ['Content-Type' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['success' => ['token', 'city', 'email']]);
            
        $this->json('POST', '/api/v1/logout', [], ['Content-Type' => 'application/json'])
            ->assertStatus(401);

    }

    public function testApiLogout() {
        $this->user = $this->createUser();
        $this->user = $this->user->toArray();
        $body = [
            'email' => $this->user['email'],
            'password' => 'password'
        ];
        $this->user = $this->json('POST','/api/v1/login', $body, ['Content-Type' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['success' => ['token', 'city', 'email']]);
            
        $this->json('POST', '/api/v1/logout', [], ['Content-Type' => 'application/json', 'Authorization' => 'Bearer '. $this->user->original['success']['token']])
            ->assertStatus(200)
            ->assertJsonStructure(['success']);

    }
}
