<?php

namespace Tests;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

	protected $faker;

	public function setUp(): void
	{
		parent::setUp();
		\Artisan::call('migrate',['-vvv' => true]);
		\Artisan::call('passport:install',['-vvv' => true]);
		\Artisan::call('db:seed',['-vvv' => true]);
		$this->faker = Factory::create();
	}
}
