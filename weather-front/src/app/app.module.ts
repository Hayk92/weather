import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './auth/register/register.component';
import {NgbModule, NgbAlertModule} from "@ng-bootstrap/ng-bootstrap";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {AuthService} from "./auth/service/auth.service";
import {HttpClientModule} from "@angular/common/http";
import {Guest} from "./auth/guard/guest";
import {Autenticated} from "./auth/guard/autenticated";
import {ApiConfig} from "./config/api-config";
import {InvalidResponse} from "./_helpers/invalid-response";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {CustomHttpInterceptor} from "./interceptor/custom-http-interceptor";
import { HomeComponent } from './weather/home/home.component';
import {UnautorizedComponent} from "./errors/unautorized/unautorized.component";
import {PageNotFoundComponent} from "./errors/page-not-found/page-not-found.component";
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import {WeatherService} from "./weather/service/weather.service";
import { HighchartsChartModule } from 'highcharts-angular';
import { CityService } from './weather/service/city.service';
import { EchoService } from './services/echo.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    HomeComponent,
    UnautorizedComponent,
    PageNotFoundComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbAlertModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HighchartsChartModule
  ],
  providers: [
    AuthService,
    WeatherService,
    CityService,
    EchoService,
    Guest,
    Autenticated,
    ApiConfig,
    InvalidResponse,
    {provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
