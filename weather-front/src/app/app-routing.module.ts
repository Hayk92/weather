import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./auth/login/login.component";
import {RegisterComponent} from "./auth/register/register.component";
import {Autenticated} from "./auth/guard/autenticated";
import {HomeComponent} from "./weather/home/home.component";
import {Guest} from "./auth/guard/guest";
import {PageNotFoundComponent} from "./errors/page-not-found/page-not-found.component";
import {UnautorizedComponent} from "./errors/unautorized/unautorized.component";
import {ForgotPasswordComponent} from "./auth/forgot-password/forgot-password.component";
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [Autenticated]},
  {path: 'login', component: LoginComponent, canActivate: [Guest]},
  {path: 'register', component: RegisterComponent, canActivate: [Guest]},
  {path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [Guest]},
  {path: 'password-reset/:token', component: ResetPasswordComponent, canActivate: [Guest]},
  {path: 'page-not-found', component: PageNotFoundComponent},
  {path: 'unauthorized', component: UnautorizedComponent},
  {path: '**', redirectTo: 'page-not-found', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
