import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/service/auth.service';
import * as io from 'socket.io-client';
import { Weather } from '../weather/class/weather';
import { resolve } from 'url';
import { reject } from 'q';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EchoService {
  public socket: any;
  private weadhers: Weather[];
  constructor(
    private auth: AuthService
  ) { 
    this.socket = window['io'] = io(`${environment.socket_url}:${environment.socket_port}`);
  }

  public privateChannel(channelName: string): Observable<any> {
     return new Observable<any> (observe => {
      this.socket.on(channelName, (data) => {
        observe.next(data.data);
      });
    });
  }
}
