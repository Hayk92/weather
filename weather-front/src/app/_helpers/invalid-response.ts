import {Injectable} from "@angular/core";

@Injectable()
export class InvalidResponse {
    get errors(): Array<any> {
        return this._errors;
    }

    set errors(value: Array<any>) {
        this._errors = value;
    }
    private _errors = [];
    constructor() {}
}
