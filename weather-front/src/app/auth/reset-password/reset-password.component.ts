import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {InvalidResponse} from "../../_helpers/invalid-response";
import {MustMatch} from "../../_helpers/must-match.validator";
import { Auth } from '../class/auth';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetForm: FormGroup;
  submitted = false;
  constructor(
      public authService: AuthService,
      public router: Router,
      public formBuilder: FormBuilder,
      private invalidResponse: InvalidResponse
  ) { }

  ngOnInit() {
    if (this.authService.authCheck){
      this.router.navigate(['/']);
      return;
    }

    this.resetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm: ['', [Validators.required, Validators.minLength(8)]]
    }, {validator: MustMatch('password', 'confirm')})
  }

  get f() {
    return this.resetForm.controls;
  }

  get err_msg() {
    return this.invalidResponse.errors;
  }

  reset(): void {
    this.submitted = true;
    if (this.resetForm.invalid) { return; }
    const email = this.resetForm.value.email;
    const password = this.resetForm.value.password;
    const password_confirmation  = this.resetForm.value.confirm;
    this.authService.resetPassword({email, password, password_confirmation} as Auth)
      .subscribe((res) => {
          if(res) {
            localStorage.setItem('weatherapp_api_token', res.success.token);
            localStorage.setItem('city', res.success.city);
            localStorage.setItem('email', res.success.email);
            this.submitted = false;
            this.router.navigate(['/']);
          }
      });
  }

}
