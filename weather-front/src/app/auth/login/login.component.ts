import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../service/auth.service";
import {Auth} from "../class/auth";
import {InvalidResponse} from "../../_helpers/invalid-response";
import {Router} from "@angular/router";
import { Weather } from 'src/app/weather/class/weather';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  constructor(
      private formBuilder: FormBuilder,
      public authService: AuthService,
      public invalidResponse: InvalidResponse,
      public router: Router
  ) { }

  ngOnInit() {
    if (this.authService.authCheck){
      this.router.navigate(['/']);
      return;
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  get f() {
    return this.loginForm.controls
  }

  get err_msg() {
    return this.invalidResponse.errors;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.loginForm.invalid){ return; }
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    this.authService.login({email, password} as Auth)
        .subscribe((res) => {
          if (res){
            localStorage.setItem('weatherapp_api_token', res.success.token);
            localStorage.setItem('city', res.success.city);
            localStorage.setItem('email', res.success.email);
            this.submitted = false;
            this.router.navigate(['/']);
          }
        },
        err => {console.log(err)})
  }

}
