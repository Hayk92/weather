export class Auth {
    id?: number;
    name?: string;
    email?: string;
    password?: string;
    api_token?: string;
    city_id?: number;
    success?: {
        city?: string;
        email?: string;
        token?: string
    }
}
