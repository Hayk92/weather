import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Auth} from "../class/auth";
import {Observable, throwError, of} from "rxjs";
import {tap, catchError} from "rxjs/internal/operators";
import {HttpHeaders} from "@angular/common/http";
import {ApiConfig} from "../../config/api-config";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      private http: HttpClient,
      private url: ApiConfig
  ) { }

  login(auth: Auth): Observable<Auth> {
    return this.http.post(`${this.url.url}/login`, auth, httpOptions)
        .pipe(
            tap((auth: Auth) => auth),
            catchError(this.handleError<Auth>('login'))
        );
  }

  register(auth: Auth): Observable<Auth> {
    return this.http.post(`${this.url.url}/register`, auth, httpOptions)
        .pipe(
            tap((auth: Auth) => auth),
            catchError(this.handleError<Auth>('register'))
        );
  }

  logout(): Observable<Auth> {
    return this.http.post(`${this.url.url}/logout`, null, httpOptions)
        .pipe(
            tap(res => res),
            catchError(this.handleError<Auth>('logout'))
        );
  }

  forgotPassword(email: Auth): Observable<Auth> {
    return this.http.post(`${this.url.url}/password/email`, email, httpOptions)
        .pipe(
            tap(res => res),
            catchError(this.handleError<Auth>('reset-email'))
        )
  }

  resetPassword(user: Auth): Observable<Auth> {
    return this.http.post(`${this.url.url}/password/reset`, user, httpOptions)
      .pipe(
        tap(res => res),
        catchError(this.handleError<Auth>('reset-password'))
      );
  }

  accessToken(): string {
    return localStorage.getItem('weatherapp_api_token');
  }

  get authCheck(): boolean {
    return this.accessToken() !== null;
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
