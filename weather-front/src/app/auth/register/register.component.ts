import { Component, OnInit } from '@angular/core';
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Auth} from "../class/auth";
import {InvalidResponse} from "../../_helpers/invalid-response";
import {MustMatch} from "../../_helpers/must-match.validator";
import { WeatherService } from 'src/app/weather/service/weather.service';
import { CityService } from 'src/app/weather/service/city.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  public cityes = [];
  constructor(
      public authService: AuthService,
      public router: Router,
      public formBuilder: FormBuilder,
      private invalidResponse: InvalidResponse,
      private cityesService: CityService
  ) { }

  ngOnInit() {
    if (this.authService.authCheck){
      this.router.navigate(['/']);
      return;
    }

    this.cities();

    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm: ['', [Validators.required, Validators.minLength(8)]],
      city: ['', [Validators.required]]
    }, {validator: MustMatch('password', 'confirm')})
  }

  get f() {
    return this.registerForm.controls;
  }

  get err_msg() {
    return this.invalidResponse.errors;
  }

  add() {
    this.submitted = true;
    if (this.registerForm.invalid) { return; }
    const name = this.registerForm.value.name;
    const email = this.registerForm.value.email;
    const password = this.registerForm.value.password;
    const password_confirmation  = this.registerForm.value.confirm;
    const city_id  = this.registerForm.value.city;
    this.authService.register({name, email, password, password_confirmation, city_id } as Auth)
        .subscribe(res => {
          if (res){
            console.log(res);
            localStorage.setItem('weatherapp_api_token', res.success.token);
            localStorage.setItem('city', res.success.city);
            localStorage.setItem('email', res.success.email);
            this.submitted = false;
            this.router.navigate(['/']);
          }
        });
  }

  cities(): void {
    this.cityesService.cities()
      .subscribe((res:any) => {this.cities = res;});
  }

  hack(val) {
    return Array.from(val);
  }

}
