import { Component, OnInit } from '@angular/core';
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Auth} from "../class/auth";
import {InvalidResponse} from "../../_helpers/invalid-response";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotForm: FormGroup;
  submitted = false;
  message = null;
  constructor(
      private authService: AuthService,
      private router: Router,
      public formBuilder: FormBuilder,
      private invalidResponse: InvalidResponse
  ) { }

  ngOnInit() {
    if (this.authService.authCheck){
      this.router.navigate(['/']);
      return;
    }

    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get f() {
    return this.forgotForm.controls;
  }

  get err_msg() {
    return this.invalidResponse.errors;
  }

  send(): void {
    this.submitted = true;
    if (this.forgotForm.invalid) { return; }
    const email = this.forgotForm.value.email;
    this.authService.forgotPassword({email} as Auth)
        .subscribe((res) => {
          if (res){
            this.message = res;
            this.submitted = false;
          }
        })
  }

}
