import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";

@Injectable()
export class ApiConfig {
    private prefix = 'api/v1';
    private baseUrl = environment.api_url;
    public url: string;
    constructor() {
        this.url = this.baseUrl + this.prefix;
    }
}
