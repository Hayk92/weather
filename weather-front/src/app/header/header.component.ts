import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth/service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
      public auth: AuthService,
      private router: Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.auth.logout()
        .subscribe(res => {
          if (res){
            localStorage.removeItem('weatherapp_api_token');
            localStorage.removeItem('city');
            localStorage.removeItem('email');
            this.router.navigate(['/login']);
          }
        })
  }

}
