import {HttpInterceptor} from "@angular/common/http";
import {HttpRequest} from "@angular/common/http";
import {HttpHandler} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {HttpEvent} from "@angular/common/http";
import {AuthService} from "../auth/service/auth.service";
import {Router} from "@angular/router";
import {catchError, repeat, retry} from "rxjs/internal/operators";
import {InvalidResponse} from "../_helpers/invalid-response";
import {Injectable} from "@angular/core";

const UNAUTHORIZED = 401;
const PAGE_NOT_FOUND = 404;
const VALIDATION_ERRORS = 422;

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor{

    constructor(
        private auth: AuthService,
        private router: Router,
        private invalidResponse: InvalidResponse
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.auth.authCheck){
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.auth.accessToken()}`
                }
            });
        }
        return next.handle(req)
            .pipe(
                retry(1),
                catchError((err) => {
                    switch (err.status) {
                        case UNAUTHORIZED:
                            this.auth.logout();
                            this.router.navigate(['login']);
                            break;
                        case PAGE_NOT_FOUND:
                            this.router.navigate(['page-not-found']);
                            break;
                        case VALIDATION_ERRORS:
                            this.invalidResponse.errors = err.error.errors;
                            break;
                    }
                    return throwError(err);
                })
            );
    }
}
