import { Component, OnInit, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import {AuthService} from "../../auth/service/auth.service";
import {Router} from "@angular/router";
import {WeatherService} from "../service/weather.service";
import {Weather} from "../class/weather";
import { HightchartService } from '../service/hightchart.service';
import { EchoService } from 'src/app/services/echo.service';
import { Subscription } from 'rxjs';
import { take, map } from 'rxjs/internal/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('charts') public chartEl: ElementRef;

  private cardinalDetections = {
    "N": [348.75, 360],
    'NNE': [11.25, 33.75],
    'NE': [33.75, 56.25],
    'ENE': [56.25, 78.75],
    'E': [78.75, 101.25],
    'ESE': [101.25, 123.75],
    'SE': [123.75, 146.25],
    'SSE': [146.25, 168.75],
    'S': [168.75, 191.25],
    'SSW': [191.25, 213.75],
    'SW': [213.75, 236.25],
    'WSW': [236.25, 258.75],
    'W': [258.75, 281.25],
    'WNW': [281.25, 303.75],
    'NW': [303.75, 326.25],
    'NNW': [326.25, 348.75]
  };

  private directions = {
    "N": "North",
    'NNE': "North-northeast",
    'NE': "Northeast",
    'ENE': "East-northeast",
    'E': "East",
    'ESE': "East-southeast",
    'SE': "Southeast",
    'SSE': "South-southeast",
    'S': "South",
    'SSW': "South-southwest",
    'SW': "Southwest",
    'WSW': "West-southwest",
    'W': "West",
    'WNW': "West-northwest",
    'NW': "Northwest",
    'NNW': "North-northwest"
  }

  weathers: Weather[];
  today: Weather;
  cities = [];
  current_city: number;
  opts = {}
  deg: number;
  sub: Subscription;
  private new_weather;
  constructor(
      public auth: AuthService,
      public router: Router,
      public weatherService: WeatherService,
      public hcs: HightchartService,
      public echoService: EchoService
  ) { }

  ngOnInit() {
    if (!this.auth.authCheck){
      this.router.navigate(['login']);
      return;
    }
    this.getCities();
    this.hcs.createChart(this.chartEl.nativeElement);
    this.current_city = +localStorage.getItem('city');
    this.getWeathers(this.current_city);
  }

  ngAfterViewInit() {
    this.initIoConnection(this.current_city);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  initIoConnection(city_id): void {
    this.sub = this.echoService.privateChannel (`weather.${city_id}`)
    .subscribe((weather) => {
        this.today = weather.forecast;
        this.weathers = weather.weathers;
        this.drawChart();
      }
    );

    this.echoService.privateChannel(`cities`)
      .subscribe((city) => {
        this.cities.forEach((val, index) => {
          if(val.id === city.cities.id){
            val = city.cities;
          }
            this.cities[index] = val;
        }, this.cities);
      });
  }

  getCities(): void {
    this.weatherService.cities()
      .subscribe((res) => {
        if(res){
          this.cities = res.cities;
        }
      })
  }

  getWeathers(city_id): void {
    let offset = new Date().getTimezoneOffset();
    this.weatherService.getWeathers({city_id, offset})
        .subscribe((res) => {
          if(res) {
            this.weathers = res.weathers;
            this.deg = res.today ? res.today.wind_deg : 0;
            this.today = res.today;
            this.drawChart();
          }
        });
  }

  switchCity(id: number) {
    this.current_city = id;
    this.getWeathers(id)
    this.initIoConnection(this.current_city);
  }

  switchDay(id: number, date: string) {
    const city = this.current_city;
    this.weatherService.getWeather({id, date, city})
      .subscribe(res => {
        if(res) {
          this.today = res;
          this.deg = res.wind_deg;
          this.drawChart();
        }
      })
  }

  get degree() {
    let result = null;
    Object.keys(this.cardinalDetections).map((key) => {
      if (this.deg >= this.cardinalDetections[key][0] && this.deg < this.cardinalDetections[key][1]) {
				result = key;
      }
    });
    return this.directions[result];
  }

  drawChart() {
    let opts = [];
    let times = []
    if(this.today && this.today.daily_forecasts){
      this.today.daily_forecasts.map(value => {
        if(value.city_id === this.current_city){
          opts.push(value.temp);
          times.push(value.date_text)
        }
      })
    }
    this.hcs.updateChart(opts, times);
  }
}
