import { TestBed } from '@angular/core/testing';

import { HightchartService } from './hightchart.service';

describe('HightchartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HightchartService = TestBed.get(HightchartService);
    expect(service).toBeTruthy();
  });
});
