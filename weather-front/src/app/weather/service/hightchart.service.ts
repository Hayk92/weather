import { Injectable } from '@angular/core';
import * as Highcharts from 'highcharts';

@Injectable({
  providedIn: 'root'
})
export class HightchartService {
  charts = null;
  defaultOptions = {
    chart: {
      type: 'areaspline'
    },
    title: {
        text: ''
    },
    xAxis: {
      type: 'datetime',
      categories: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00']
    },
    tooltip: {
        formatter: function() {
            return  `Temp <strong>(${Number(this.y).toFixed(2)})</strong> <br>
                     For <strong>${this.x}</strong>`
        }
    },
    yAxis: {
      tickInterval: 1,
      title: {
        text: "Celsius"
      }
    },
    subtitle: {
        text: ''
    },
    legend: {
        enabled: false
    },
    series: [{
      data: []
    }]
  }

  constructor() {
  }
  
  createChart(container, options?: any) {
    let opts = !!options ? options : this.defaultOptions;
    let e = document.createElement("div");
    
    container.appendChild(e);
    
    if(!!opts.chart) {
      opts.chart['renderTo'] = e;
    }
    else {
      opts.chart = {
        'renderTo': e
      }
    }
    this.charts = new Highcharts.Chart(opts);
  }
  
  removeFirst() {
    this.charts.shift();
  }
  
  removeLast() {
    this.charts.pop();
  }
  
  getChartInstances(): number {
    return this.charts.length;
  }

  getCharts() {
    return this.charts;
  }

  updateChart(data, times) {
    let opt: any = this.defaultOptions;
    let dt = [];
    opt.series[0].data = data;
    times.forEach(element => {
      dt.push(this.convertToLocalTime(element));
    });
    opt.xAxis.categories = dt;
    this.charts = new Highcharts.Chart(opt);
  }

  convertToLocalTime(d_t) {
    let date = new Date(d_t),
        miliseconds = date.getTime();
        
    const hours   = `0${new Date(miliseconds).getHours()}`.slice(-2);
    const minutes = `0${new Date(miliseconds).getMinutes()}`.slice(-2);

    return `${hours}:${minutes}`
  }

  get userTimezoneOffset()
  {
    let offset = new Date().getTimezoneOffset();
    return ((offset / 60) * 3600000);
  }
}
