import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {Weather} from "../class/weather";
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from "@angular/common/http";
import {ApiConfig} from "../../config/api-config";
import {tap, catchError} from "rxjs/internal/operators";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(
    private http: HttpClient,
    private url: ApiConfig
  ) { }

  cities(): Observable<any> {
    return this.http.get(`${this.url.url}/cities`, httpOptions)
      .pipe(
        tap((res:any) => res),
        catchError(this.handleError('cities'))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
