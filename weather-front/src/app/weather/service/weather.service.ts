import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {Weather} from "../class/weather";
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from "@angular/common/http";
import {ApiConfig} from "../../config/api-config";
import {tap, catchError} from "rxjs/internal/operators";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
      private http: HttpClient,
      private url: ApiConfig
  ) { }

  cities(): Observable<any> {
    let offset = new Date().getTimezoneOffset();
    return this.http.get(`${this.url.url}/home?offset=${offset}`, httpOptions)
      .pipe(
        tap(res => res),
        catchError(this.handleError<Weather[]>('home'))
      );
  }
  getWeathers(city_id): Observable<any> {
    return this.http.post(`${this.url.url}/weathers`, city_id, httpOptions)
        .pipe(
            tap((res: any) => res),
            catchError(this.handleError<any>('weathers'))
        );
  }

  getWeather(data): Observable<Weather> {
    let offset = new Date().getTimezoneOffset();
    return this.http.get(`${this.url.url}/home/${data.id}?day=${data.date}&city=${data.city}&offset=${offset}`, httpOptions)
        .pipe(
            tap((res: Weather) => res),
            catchError(this.handleError<Weather>('weather'))
        );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
