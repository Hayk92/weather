let app     = require('express')(),
    server  = require('http').Server(app),
    io      = require('socket.io')(server),
    config  = require('./socket_config');
    Redis   = require('ioredis');

server.listen(config.port);

io.on('connection', (socket) => {
    console.log('client connected');
});

let redis = new Redis();
redis.psubscribe('*', function(err, count) {
    //
});

redis.on('pmessage', (subscribedm, channel, message) => {
    message = JSON.parse(message);
    io.emit(channel, message);
});

io.on('disconnect', () => {
    redis.quit();
});
